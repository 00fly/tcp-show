# TCP协议示例

## 一.业务需求
1. tcp client主动连接tcp server
2. tcp server下发命令到tcp client执行
3. 执行结果回调


```mermaid
graph LR
C1(tcp client)
C2(tcp client)
C3(tcp client)
S1(tcp server)

 C1 --> S1  
 C2 --> S1  
 C3 --> S1
```

## 二.技术难点

tcp server服务器controller通过tcp协议下发命令到tcp client执行，controller中如何获取执行结果？


## 三.打包镜像

`mvn package 已经绑定docker:build,无需显示调用`

```shell
#server打包
mvn clean package

#server打包并推送
mvn clean package docker:push

#client打包
mvn clean package -f pom-client.xml

#client打包并推送
mvn clean package docker:push -f pom-client.xml
```