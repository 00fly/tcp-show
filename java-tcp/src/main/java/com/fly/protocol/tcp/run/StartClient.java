package com.fly.protocol.tcp.run;

import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;

import com.fly.protocol.tcp.bio.TcpClient;

public class StartClient
{
    public static void main(String[] args)
    {
        // docker环境下优先使用docker-compose中environment值
        String serverIp = StringUtils.defaultIfBlank(System.getenv().get("TCP_SERVER"), "127.0.0.1");
        IntStream.rangeClosed(1, 3).forEach(i -> {
            TcpClient client = new TcpClient("CLIENT_" + i);
            if (client.connectServer(serverIp, 8000))
            {
                new Thread(client).start();
            }
        });
    }
}
