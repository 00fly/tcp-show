package com.fly.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApiControllerTest
{
    private static RestTemplate restTemplate;
    
    static
    {
        // 配置proxy，connectTimeout，readTimeout等参数
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setConnectTimeout(1000);
        requestFactory.setReadTimeout(1000);
        restTemplate = new RestTemplate(requestFactory);
    }
    
    @Test
    public void testJsonRequestBody2()
        throws IOException
    {
        String url = "http://192.168.114.250:8080/api/post";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        Resource resource = new ClassPathResource("data/json");
        String text = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);
        HttpEntity<String> requestEntity = new HttpEntity<>(text, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestEntity, String.class);
        log.info("******  ResponseEntity body: {}", responseEntity.getBody());
    }
}
