package com.fly.tcp.entity;

import java.util.UUID;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Command
{
    /**
     * 命令id
     */
    private String commandId = UUID.randomUUID().toString();
    
    /**
     * 客户端id
     */
    private String clientId;
    
    /**
     * 具体命令内容
     */
    private String text;
}
